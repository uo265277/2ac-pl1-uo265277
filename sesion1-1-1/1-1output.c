#include <stdio.h>

int main()
{
    int i = 3;
    float f = 2.4;
    char * s = "Content";

    // Print the content of i
	printf("Valor de i: %d\n", i);
    // Print the content of f
	printf("Valor de f: %f\n", f);
	
    // Print the content of the string
	printf("Valor de cadena: %s\n", s);


    return 0;
}