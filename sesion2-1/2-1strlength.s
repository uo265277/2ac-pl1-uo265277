.data
cadena: .asciiz "hola"
result: .word 0

.text
main:
	xor r16,r16,r16
	xor r17,r17,r17
loop:
	lbu r4, cadena(r17)
	beqz r4, end ; si estas al final de la cadena salta al final de bucle
	jal procedimiento
	dadd r16, r16, r16
	daddi r17,r17,1 ; paso al siguiente elemento de la cadena
	j loop
end:
	sd r16, result (r0) ; mete en el resultado el numero de r16
	halt

procedimiento:
	daddi r2, r0, 1
	j r31